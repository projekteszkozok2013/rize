class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.string   :name,       limit: 40, null: false
      t.string   :short_name, limit: 2,  null: false
    end

    change_table :types do |t|
      t.index    :name,       unique: true
      t.index    :short_name, unique: true
    end
  end
end
