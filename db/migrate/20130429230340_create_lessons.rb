class CreateLessons < ActiveRecord::Migration
  def change
    create_table   :lessons do |t|
      t.references :student,    null: false
      t.references :semester,   null: false
      t.references :topic,      null: false
      t.references :klass,      null: false
      t.references :repeated

      t.text     :evaluation,   limit: 10.kilobytes
      t.integer  :grade
      t.datetime :finalized_at

      t.datetime :created_at
      t.datetime :updated_at
    end

    change_table :lessons do |t|
      t.index [ :student_id, :klass_id ], unique: true
      t.index   :semester_id
      t.index   :topic_id
      t.index   :finalized_at
    end
  end
end
