class CreateKlasses < ActiveRecord::Migration
  def change
    create_table   :klasses do |t|
      t.references :location,    null: false
      t.references :teacher,     null: false
      t.datetime   :kept_on,     null: false
      t.string     :description,              limit: 50
    end

    change_table :klasses do |t|
      t.index  [ :location_id, :teacher_id, :kept_on ], unique: true
    end
  end
end
