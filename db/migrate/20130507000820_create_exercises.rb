class CreateExercises < ActiveRecord::Migration
  def change
    create_table   :exercises do |t|
      t.references :lesson,        null: false
      t.references :type,          null: false
      t.references :reserved_by

      t.datetime   :deadline_at
      t.datetime   :solved_at
      t.string     :solution_path, limit: 200
      t.datetime   :reserved_at
      t.text       :evaluation,    limit: 10.kilobytes
      t.integer    :grade
      t.datetime   :finalized_at

      t.datetime   :created_at
      t.datetime   :updated_at
    end

    change_table :exercises do |t|
      t.index    :lesson_id
      t.index    :type_id
      t.index    :deadline_at
      t.index    :solved_at
      t.index  [ :reserved_at, :reserved_by_id ]
      t.index    :finalized_at
    end
  end
end
