class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string   :building,  limit: 5,  null: false
      t.string   :floor,     limit: 15, null: false
      t.string   :room,      limit: 15, null: false
      t.integer  :capacity,             null: false
    end

    change_table :locations do |t|
      t.index  [ :building, :floor, :room ], unique: true
    end
  end
end
