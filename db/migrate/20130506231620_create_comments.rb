class CreateComments < ActiveRecord::Migration
  def change
    create_table   :comments do |t|
      t.references :commentable, null: false, polymorphic: true
      t.references :user,        null: false

      t.text       :content,     null: false, limit: 10.kilobytes

      t.datetime   :created_at
      t.datetime   :updated_at
    end

    change_table :comments do |t|
      t.index   [:commentable_id, :commentable_type]
    end
  end
end
