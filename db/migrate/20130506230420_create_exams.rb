class CreateExams < ActiveRecord::Migration
  def change
    create_table   :exams do |t|
      t.references :lesson, null: false

      t.integer    :grade
      t.datetime   :finalized_at

      t.datetime   :created_at
      t.datetime   :updated_at
    end

    change_table :exams do |t|
      t.index    :lesson_id, unique: true
      t.index    :finalized_at
    end
  end
end
