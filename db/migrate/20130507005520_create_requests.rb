class CreateRequests < ActiveRecord::Migration
  def change
    create_table   :requests do |t|
      t.references :lesson,        null: false
      t.references :source,        null: false
      t.references :destination,   null: false

      t.datetime :source_accepted_at
      t.datetime :destination_accepted_at
    end

    change_table :requests do |t|
      t.index [:lesson_id, :source_id, :destination_id], unique: true
    end
  end
end
