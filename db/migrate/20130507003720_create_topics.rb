class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string   :name,  limit: 15, null: false
    end

    change_table :topics do |t|
      t.index    :name,  unique: true
    end
  end
end
