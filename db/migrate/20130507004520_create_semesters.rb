class CreateSemesters < ActiveRecord::Migration
  def change
    create_table :semesters do |t|
      t.integer :year,   null: false
      t.integer :period, null: false
    end

    change_table :semesters do |t|
      t.index [ :year, :period ], unique: true
    end
  end
end
