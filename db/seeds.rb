# encoding: utf-8
ActiveRecord::Base.transaction do

  roles = %w{Student Evaluator LabLeader Admin}.each do |role|
    Role.create! name: role
  end

  teszt_student    = User.create!(name: 'Teszt Hallgató', email: 'student@rize.com', password: 'asdfasdf').add_role :student
  teszt_evaluator  = User.create!(email: 'evaluator@rize.com', password: 'asdfasdf').add_role :evaluator
  teszt_lab_leader = User.create!(email: 'lab_leader@rize.com', password: 'asdfasdf').add_role :lab_leader
  teszt_admin      = User.create!(email: 'admin@rize.com', password: 'asdfasdf').add_role :admin

  # Topics
  sql    = FactoryGirl.create :topic, :name => 'sql'
  oracle = FactoryGirl.create :topic, :name => 'oracle'
  java   = FactoryGirl.create :topic, :name => 'java'
  xsql   = FactoryGirl.create :topic, :name => 'xsql'
  asdql  = FactoryGirl.create :topic, :name => 'asdsql'

  # Klasses
  husz         = FactoryGirl.create :klass, :kept_on => '2013-06-20 00:00', :teacher => teszt_lab_leader
  huszonegy    = FactoryGirl.create :klass, :kept_on => '2013-06-21 00:00', :teacher => teszt_lab_leader
  huszonketto  = FactoryGirl.create :klass, :kept_on => '2013-06-22 00:00', :teacher => teszt_lab_leader
  huszonharom  = FactoryGirl.create :klass, :kept_on => '2013-06-23 00:00', :teacher => teszt_lab_leader
  huszonnegy   = FactoryGirl.create :klass, :kept_on => '2013-06-24 00:00', :teacher => teszt_lab_leader

  # Lessons
  hallgatosql    = FactoryGirl.create  :lesson, :topic => sql,    :student => teszt_student,  :klass => husz
  hallgatooracle = FactoryGirl.create  :lesson, :topic => oracle, :student => teszt_student,  :klass => huszonegy
  hallgatojava   = FactoryGirl.create  :lesson, :topic => java,   :student => teszt_student,  :klass => huszonketto
  hallgatoxsql   = FactoryGirl.create  :lesson, :topic => xsql,   :student => teszt_student,  :klass => huszonharom
  hallgatoasdql  = FactoryGirl.create  :lesson, :topic => asdql,  :student => teszt_student,  :klass => huszonnegy

  # Exams
  hallgatosqlex  = FactoryGirl.create :exam, :lesson => hallgatosql

  # Exercises
  exercise1 = FactoryGirl.create :exercise, :reserved_by => teszt_evaluator, :lesson => hallgatosql
  exercise2 = FactoryGirl.create :exercise, :lesson => hallgatoxsql
  exercise3 = FactoryGirl.create :exercise, :lesson => hallgatooracle
  exercise4 = FactoryGirl.create :exercise, :lesson => hallgatojava
  exercise5 = FactoryGirl.create :exercise, :lesson => hallgatoasdql

end
