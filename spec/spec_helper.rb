# Figure out where we are being loaded from
if $LOADED_FEATURES.grep(/spec\/spec_helper\.rb/).any?
  begin
    raise "foo"
  rescue => e
    puts <<-MSG
===================================================
  It looks like spec_helper.rb has been loaded
  multiple times. Normalize the require to:

    require "spec/spec_helper"

  Things like File.join and File.expand_path will
  cause it to be loaded multiple times.

  Loaded this time from:

    #{e.backtrace.join("\n    ")}
  ===================================================
MSG
  end
end

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'

if ENV['RAILS_ENV'] == 'test'
  require 'simplecov'
  SimpleCov.start 'rails'
end

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'

require 'rspec/mocks'
require 'capybara/rspec'
require 'selenium-webdriver'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  config.use_transactional_fixtures = true
  config.infer_base_class_for_anonymous_controllers = true
  config.order = "random"

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
  config.treat_symbols_as_metadata_keys_with_true_values = true

  config.include Capybara::DSL
  # config.include ParanoidMatcher
  # config.include KaminariMatcher
  # config.include RSpec::TagMatchers

  # config.before :suite do
  #   DatabaseCleaner.strategy = :transaction
  #   DatabaseCleaner.clean_with :truncation
  # end

  # config.before type: :request do
  #   DatabaseCleaner.strategy = :truncation
  # end

  # config.before do
  #   DatabaseCleaner.start
  #   #WebMock.disable_net_connect!(:allow_localhost => true)
  #   ActionMailer::Base.deliveries.clear
  # end

  # config.after do
  #   DatabaseCleaner.clean
  # end

  #Capybara.javascript_driver = :webkit
  Capybara.ignore_hidden_elements = false
  Capybara.default_selector       = :css
  Capybara.default_wait_time      = 5
end

