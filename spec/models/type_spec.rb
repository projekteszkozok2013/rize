# encoding: utf-8
require 'spec_helper'

describe Type do
  # Accessible attributes
  it { should allow_mass_assignment_of(:name) }
  it { should allow_mass_assignment_of(:short_name) }

  # Associations
  it { should have_many(:exercises) }

  # Name
  it { should have_db_column(:name).of_type(:string) }
  it { should validate_presence_of(:name) }
  it { should ensure_length_of(:name).is_at_most(40) }

  # Short Name
  it { should have_db_column(:short_name).of_type(:string) }
  it { should validate_presence_of(:short_name) }
  it { should ensure_length_of(:short_name).is_at_most(2) }

  # Indexes and uniqueness
  it { should have_db_index(:name).unique(true) }
  it { should have_db_index(:short_name).unique(true) }
end
