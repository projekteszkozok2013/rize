# encoding: utf-8
require 'spec_helper'

describe Topic do
  # Accessible attributes
  it { should allow_mass_assignment_of(:name) }

  # Associations
  it { should have_many(:lessons) }

  # Name
  it { should have_db_column(:name) }
  it { should validate_presence_of(:name) }
  it { should ensure_length_of(:name).is_at_most(15) }

  # Indexes and uniqueness
  it { should have_db_index(:name).unique(true) }
end
