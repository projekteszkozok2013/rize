# encoding: utf-8
require 'spec_helper'

describe Comment do
  # Accessible attributes
  it { should allow_mass_assignment_of(:commentable) }
  it { should allow_mass_assignment_of(:user) }
  it { should allow_mass_assignment_of(:content) }

  it { should_not allow_mass_assignment_of(:commentable_id) }
  it { should_not allow_mass_assignment_of(:commentable_type) }
  it { should_not allow_mass_assignment_of(:user_id) }

  # Associations
  it { should belong_to(:commentable) }
  it { should belong_to(:user) }

  # Commentable Id
  it { should have_db_column(:commentable_id).of_type(:integer) }
  it { should validate_presence_of(:commentable_id) }
  it { should validate_numericality_of(:commentable_id) }

  # Commentable Type
  it { should have_db_column(:commentable_type).of_type(:string) }
  it { should validate_presence_of(:commentable_type) }

  # User Id
  it { should have_db_column(:user_id).of_type(:integer) }
  it { should validate_presence_of(:user_id) }
  it { should validate_numericality_of(:user_id) }

  # Content
  it { should have_db_column(:content).of_type(:text) }
  it { should validate_presence_of(:content) }
  it { should ensure_length_of(:content).is_at_most(10.kilobytes) }

  # Indexes and uniqueness
  it { should have_db_index([:commentable_id, :commentable_type]) }
end
