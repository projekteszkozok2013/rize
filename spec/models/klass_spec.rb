# encoding: utf-8
require 'spec_helper'

describe Klass do
  # Accessible attributes
  it { should allow_mass_assignment_of(:location) }
  it { should allow_mass_assignment_of(:teacher) }
  it { should allow_mass_assignment_of(:kept_on) }
  it { should allow_mass_assignment_of(:description) }

  it { should_not allow_mass_assignment_of(:location_id) }
  it { should_not allow_mass_assignment_of(:teacher_id) }

  # Associations
  it { should belong_to(:location) }
  it { should belong_to(:teacher) }

  it { should have_many(:lessons) }

  # Location Id
  it { should have_db_column(:location_id).of_type(:integer) }
  it { should validate_presence_of(:location_id) }
  it { should validate_numericality_of(:location_id) }

  # Teacher Id
  it { should have_db_column(:teacher_id).of_type(:integer) }
  it { should validate_presence_of(:teacher_id) }
  it { should validate_numericality_of(:teacher_id) }

  # Kept On
  it { should have_db_column(:kept_on).of_type(:datetime) }
  it { should validate_presence_of(:kept_on) }

  # Description
  it { should have_db_column(:description).of_type(:string) }
  it { should ensure_length_of(:description).is_at_most(50) }

  # Indexes and uniqueness
  it { should have_db_index([:location_id, :teacher_id, :kept_on]).unique(true) }
end
