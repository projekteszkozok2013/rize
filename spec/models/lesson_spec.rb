# encoding: utf-8
require 'spec_helper'

describe Lesson do
  # Accessible attributes
  it { should allow_mass_assignment_of(:topic)    }
  it { should allow_mass_assignment_of(:klass)    }
  it { should allow_mass_assignment_of(:student)  }
  it { should allow_mass_assignment_of(:semester) }
  it { should allow_mass_assignment_of(:evaluation) }

  it { should_not allow_mass_assignment_of(:topic_id)    }
  it { should_not allow_mass_assignment_of(:klass_id)    }
  it { should_not allow_mass_assignment_of(:student_id)  }
  it { should_not allow_mass_assignment_of(:semester_id) }
  it { should_not allow_mass_assignment_of(:repeated_id) }

  # Associations
  it { should belong_to(:topic)    }
  it { should belong_to(:klass)    }
  it { should belong_to(:student)  }
  it { should belong_to(:semester) }
  it { should belong_to(:repeated) }

  # Student Id
  it { should have_db_column(:student_id).of_type(:integer) }
  it { should validate_presence_of(:student_id) }
  it { should validate_numericality_of(:student_id) }

  # Semester Id
  it { should have_db_column(:semester_id).of_type(:integer) }
  it { should validate_presence_of(:semester_id) }
  it { should validate_numericality_of(:semester_id) }

  # Topic Id
  it { should have_db_column(:topic_id).of_type(:integer) }
  it { should validate_presence_of(:topic_id) }
  it { should validate_numericality_of(:topic_id) }

  # Klass Id
  it { should have_db_column(:klass_id).of_type(:integer) }
  it { should validate_presence_of(:klass_id) }
  it { should validate_numericality_of(:klass_id) }

  # Repeated Id
  # it { should have_db_column(:repeated_id).of_type(:integer) }
  # it { should validate_numericality_of(:repeated_id) }

  # Evaluation
  it { should have_db_column(:evaluation).of_type(:text) }
  it { should ensure_length_of(:evaluation).is_at_most(10.kilobytes) }

  # Grade
  it { should have_db_column(:grade).of_type(:integer) }
  it { should validate_numericality_of(:grade) }

  # Finalized At
  it { should have_db_column(:finalized_at).of_type(:datetime) }

  # Indexes and uniqueness
  it { should have_db_index([:student_id, :klass_id]).unique(true) }
end
