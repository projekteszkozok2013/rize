# encoding: utf-8
require 'spec_helper'

describe Request do
  # Accessible attributes
  it { should allow_mass_assignment_of(:lesson) }
  it { should allow_mass_assignment_of(:source) }
  it { should allow_mass_assignment_of(:destination) }
  it { should allow_mass_assignment_of(:source_accepted_at) }
  it { should allow_mass_assignment_of(:destination_accepted_at) }

  it { should_not allow_mass_assignment_of(:lesson_id) }
  it { should_not allow_mass_assignment_of(:source_id) }
  it { should_not allow_mass_assignment_of(:destination_id) }

  # Associations
  it { should belong_to(:lesson) }
  it { should belong_to(:source) }
  it { should belong_to(:destination) }

  it { should have_many(:comments) }

  # Lesson Id
  it { should have_db_column(:lesson_id) }
  it { should validate_presence_of(:lesson_id) }
  it { should validate_numericality_of(:lesson_id) }

  # Source Id
  it { should have_db_column(:source_id) }
  it { should validate_presence_of(:source_id) }
  it { should validate_numericality_of(:source_id) }

  # Destination Id
  it { should have_db_column(:destination_id) }
  it { should validate_presence_of(:destination_id) }
  it { should validate_numericality_of(:destination_id) }

  # Source Accepted At
  it { should have_db_column(:source_accepted_at) }

  # Destination Accepted At
  it { should have_db_column(:destination_accepted_at) }

  # Indexes and uniqueness
  it { should have_db_index([:lesson_id, :source_id, :destination_id]).unique(true) }
end
