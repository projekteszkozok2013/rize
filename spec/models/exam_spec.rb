# encoding: utf-8
require 'spec_helper'

describe Exam do
  # Accessible attributes
  it { should allow_mass_assignment_of(:lesson) }
  it { should allow_mass_assignment_of(:grade) }
  it { should allow_mass_assignment_of(:finalized_at) }

  it { should_not allow_mass_assignment_of(:lesson_id) }

  # Associations
  it { should belong_to(:lesson) }

  it { should have_many(:comments) }

  # Lesson Id
  it { should have_db_column(:lesson_id).of_type(:integer) }
  it { should validate_presence_of(:lesson_id) }
  it { should validate_uniqueness_of(:lesson_id) }
  it { should validate_numericality_of(:lesson_id) }

  # Grade
  it { should have_db_column(:grade).of_type(:integer) }
  it { should validate_numericality_of(:grade) }

  # Finalized At
  it { should have_db_column(:finalized_at).of_type(:datetime) }

  # Indexes and uniqueness
  it { should have_db_index(:lesson_id).unique(true) }
  it { should have_db_index(:finalized_at) }
end
