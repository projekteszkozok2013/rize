# encoding: utf-8
require 'spec_helper'

describe User do
  # Accessible attributes
  it { should allow_mass_assignment_of(:name) }
  it { should allow_mass_assignment_of(:email) }
  it { should allow_mass_assignment_of(:password) }
  it { should allow_mass_assignment_of(:password_confirmation) }
  it { should allow_mass_assignment_of(:remember_me) }

  # Associations
  it { should have_and_belong_to_many(:roles) }
end
