# encoding: utf-8
require 'spec_helper'

describe Semester do
  # Accessible attributes
  it { should allow_mass_assignment_of(:year) }
  it { should allow_mass_assignment_of(:period) }

  # Associations
  it { should have_many(:lessons) }

  # Year
  it { should have_db_column(:year) }
  it { should validate_presence_of(:year) }
  it { should validate_numericality_of(:year) }

  # Period
  it { should have_db_column(:period) }
  it { should validate_presence_of(:period) }
  it { should validate_numericality_of(:period) }

  # Indexes and uniqueness
  it { should have_db_index([:year, :period]).unique(true) }
end
