# encoding: utf-8
require 'spec_helper'

describe Exercise do
  # Accessible attributes
  it { should allow_mass_assignment_of(:lesson) }
  it { should allow_mass_assignment_of(:type) }
  it { should allow_mass_assignment_of(:deadline_at) }
  it { should allow_mass_assignment_of(:solved_at) }
  it { should allow_mass_assignment_of(:solution_path) }
  it { should allow_mass_assignment_of(:reserved_at) }
  it { should allow_mass_assignment_of(:reserved_by) }
  it { should allow_mass_assignment_of(:evaluation) }
  it { should allow_mass_assignment_of(:grade) }
  it { should allow_mass_assignment_of(:finalized_at) }

  it { should_not allow_mass_assignment_of(:lesson_id) }
  it { should_not allow_mass_assignment_of(:type_id) }

  # Associations
  it { should belong_to(:lesson) }
  it { should belong_to(:type) }

  it { should have_many(:comments) }

  # Lesson Id
  it { should have_db_column(:lesson_id).of_type(:integer) }
  it { should validate_presence_of(:lesson_id) }
  it { should validate_numericality_of(:lesson_id) }

  # Type Id
  it { should have_db_column(:type_id).of_type(:integer) }
  it { should validate_presence_of(:type_id) }
  it { should validate_numericality_of(:type_id) }

  # Type Id
  it { should have_db_column(:type_id).of_type(:integer) }
  it { should validate_presence_of(:type_id) }
  it { should validate_numericality_of(:type_id) }

  # Deadline At
  it { should have_db_column(:deadline_at).of_type(:datetime) }

  # Solved At
  it { should have_db_column(:solved_at).of_type(:datetime) }

  # Solution Path
  it { should have_db_column(:solution_path).of_type(:string) }
  it { should ensure_length_of(:solution_path).is_at_most(200) }

  # Reserved At
  it { should have_db_column(:reserved_at).of_type(:datetime) }

  # Evaluation
  it { should have_db_column(:evaluation).of_type(:text) }
  it { should ensure_length_of(:evaluation).is_at_most(10.kilobytes) }

  # Grade
  it { should have_db_column(:grade).of_type(:integer) }
  it { should validate_numericality_of(:grade) }

  # Finalized At
  it { should have_db_column(:finalized_at).of_type(:datetime) }

  # Indexes and uniqueness
  it { should have_db_index(:lesson_id) }
  it { should have_db_index(:type_id) }
  it { should have_db_index(:deadline_at) }
  it { should have_db_index(:solved_at) }
  it { should have_db_index(:finalized_at) }
  it { should have_db_index([:reserved_at, :reserved_by_id]) }
end
