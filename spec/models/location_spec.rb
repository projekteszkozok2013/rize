# encoding: utf-8
require 'spec_helper'

describe Location do
  # Accessible attributes
  it { should allow_mass_assignment_of(:building) }
  it { should allow_mass_assignment_of(:floor) }
  it { should allow_mass_assignment_of(:room) }
  it { should allow_mass_assignment_of(:capacity) }

  # Associations
  it { should have_many(:klasses) }

  # Building
  it { should have_db_column(:building).of_type(:string) }
  it { should validate_presence_of(:building) }
  it { should ensure_length_of(:building).is_at_most(5) }

  # Floor
  it { should have_db_column(:floor).of_type(:string) }
  it { should validate_presence_of(:floor) }
  it { should ensure_length_of(:floor).is_at_most(15) }

  # Room
  it { should have_db_column(:room).of_type(:string) }
  it { should validate_presence_of(:room) }
  it { should ensure_length_of(:room).is_at_most(15) }

  # Indexes and uniqueness
  it { should have_db_index([:building, :floor, :room]).unique(true) }

  # Capacity
  it { should have_db_column(:capacity).of_type(:integer) }
  it { should validate_presence_of(:capacity) }
  it { should validate_numericality_of(:capacity) }
end
