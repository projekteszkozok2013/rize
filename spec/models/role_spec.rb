# encoding: utf-8
require 'spec_helper'

describe Role do
  # Accessible attributes
  it { should allow_mass_assignment_of(:name) }

  # Associations
  it { should have_and_belong_to_many(:users) }

  # Content
  it { should have_db_column(:name).of_type(:string) }
  it { should validate_presence_of(:name) }
end
