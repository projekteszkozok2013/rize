# encoding: utf-8

FactoryGirl.define do
  factory :user do
    name                  "Teszt Felhasználó"
    email                 "teszt@teszt.hu"
    password              "password"
    password_confirmation "password"
  end
end
