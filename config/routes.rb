Rize::Application.routes.draw do
  devise_for :users, only: [:sessions, :passwords]

  root to: 'application#root'
  get '/dashboard' => 'dashboard#show', as: :dashboard

  # Lessons
  get '/lessons'    => 'lessons#index', as: :lessons
  get '/lesson/:id' => 'lessons#show',  as: :lesson
  put '/lesson/:id' => 'lessons#update'

  # Exercises for lesson
  get '/lessons/:lesson_id/exercises' => 'exercises#index', :as => :lesson_exercises

  # Comments for lesson
  get '/lessons/:lesson_id/comments' => 'comments#index', :as => :lesson_comments
  post '/lessons/:lesson_id/comments' => 'comments#create'

  # Comments for exercise
  get '/exercises/:exercise_id/comments' => 'comments#index', :as => :exercise_comments
  post '/exercises/:exercise_id/comments' => 'comments#create'

  # Exams
  get '/exams'     => 'exams#index', as: :exams
  get '/exams/:id' => 'exams#show',  as: :exam
  put '/exam/:id'  => 'exams#update'

  # Lesson for evaluator
   get '/klasses/:klass_id/lessons' => 'lessons#index', :as => :students


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
