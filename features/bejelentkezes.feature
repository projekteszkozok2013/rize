# encoding: utf-8
# language: hu

Jellemző: bejelentkezés

  Azért, hogy a rendszert használhassam,
  Egyszerű felhasználóként,
  Be kell tudnom jelentkezni a Rize rendszerbe.

  Háttér:
    Amennyiben adottak a rendszerben felhasználók

  Forgatókönyv: bejelentkezés jó email-címmel és jelszóval
    Amennyiben a bejelentkezés oldalon vagyok
    Ha beírom a felhasználónevem
    És beírom a jelszavam
    Majd megnyomom a bejelentkezés gombot
    Akkor az áttekintés oldalra kell jutnom

  Forgatókönyv: belépés a rendszerbe azonosító információk nélkül
    Amennyiben nincsen semmilyen azonosító információm
    Ha a főoldalra megyek
    Akkor a bejelentkezés oldalra kell jutnom

  Forgatókönyv: bejelentkezés rossz email-címmel és jelszóval
    Amennyiben a bejelentkezés oldalon vagyok
    Ha beírok egy rossz felhasználónevet
    És beírok egy rossz jelszót
    Majd megnyomom a bejelentkezés gombot
    Akkor ismét a bejelentkezés oldalon kell lennem

