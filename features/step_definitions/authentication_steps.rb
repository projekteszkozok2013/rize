# encoding: utf-8

Amennyiben /^adottak a rendszerben felhasználók$/ do
  FactoryGirl.create :user
end

Ha /^beírom a felhasználónevem$/ do
  fill_in :user_email, with: 'teszt@teszt.hu'
end

Ha(/^beírom a jelszavam$/) do
  fill_in :user_password, with: 'password'
end

Majd(/^megnyomom a bejelentkezés gombot$/) do
  click_button "Sign in"
end

Akkor(/^az áttekintés oldalra kell jutnom$/) do
  current_path.should == dashboard_path
end

Amennyiben(/^nincsen semmilyen azonosító információm$/) do
end

Ha(/^a főoldalra megyek$/) do
  visit root_path
end

Akkor(/^a bejelentkezés oldalra kell jutnom$/) do
  current_path.should == new_user_session_path
end

Ha(/^beírok egy rossz felhasználónevet$/) do
  fill_in :user_email, with: 'rossz@teszt.hu'
end

Ha(/^beírok egy rossz jelszót$/) do
  fill_in :user_password, with: 'asdf'
end

Akkor(/^ismét a bejelentkezés oldalon kell lennem$/) do
  current_path.should == new_user_session_path
end

