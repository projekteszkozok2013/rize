# encoding: utf-8

Amennyiben /^(?:az?) (.*) oldalon vagyok$/ do |title|
  visit resolv(title)
end

def resolv title
  case title
  when /^bejelentkezés$/i
    return new_user_session_path
  else
    raise ArgumentError, "Definiáld a #{__FILE__}@#{__LINE__-2}:\#resolv metódusban a '#{title}' kifejezést!"
  end
end
