class DashboardController < ApplicationController
  before_filter :authenticate_user!

  def show
    redirect_to(lessons_path) if current_user.role? :student or current_user.role? :evaluator
  end
end
