class ApplicationController < ActionController::Base
  protect_from_forgery

  def root
    redirect_to(dashboard_path) and return if user_signed_in?
    redirect_to(new_user_session_path) and return
  end
end
