# encoding: utf-8
class LessonsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @lessons = Lesson.all
    return index_teacher if current_user.role? :evaluator
    return index_student if current_user.role? :student
  end

  def show
    @lesson = Lesson.find params[:id]
  end

  def update
    @lesson = Lesson.find params[:id]

    Lesson.transaction do
      if params[:lesson] and params[:lesson][:grade]
        @lesson.grade = params[:lesson][:grade]
      elsif params[:finalize] == "true" and @lesson.grade
        @lesson.finalized_at = Time.now.utc
      end

      @lesson.save!
    end

    redirect_to students_path(@lesson)
  end

  private
  def index_teacher
    render 'index_teacher'
  end

  def index_student
    render 'index_student'
  end
end
