# encoding: utf-8

class ExamsController < ApplicationController

  def index
  end

  def show
    @exam = Exam.find params[:id]
  end

  def update
    @exam = Exam.find params[:id]
  end

end
