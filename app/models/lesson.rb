# encoding: utf-8
class Lesson < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :topic, :klass, :student, :semester, :evaluation

  # Associations
  has_one    :exam
  has_many   :exercises
  belongs_to :student, class_name: 'User'
  belongs_to :semester
  belongs_to :topic
  belongs_to :klass
  belongs_to :repeated

  # Student Id
  validates :student_id,
    presence: true,
    numericality: true

  # Semester Id
  validates :semester_id,
    presence: true,
    numericality: true

  # Topic Id
  validates :topic_id,
    presence: true,
    numericality: true

  # Klass Id
  validates :klass_id,
    presence: true,
    numericality: true

  # Evaluation
  validates :evaluation,
    length: {maximum: 10.kilobytes}

  # Grade
  validates :grade,
    allow_blank: true,
    numericality: true

  def to_s
    "#{topic.name}"
  end
end
