# encoding: utf-8
class Request < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :lesson, :source, :destination, :source_accepted_at, :destination_accepted_at

  # Associations
  belongs_to :lesson
  belongs_to :source,      class_name: 'Klass'
  belongs_to :destination, class_name: 'Klass'

  has_many :comments, as: :commentable

  # Validations
  # Lesson Id
  validates :lesson_id,
    presence: true,
    numericality: true

  # Source Id
  validates :source_id,
    presence: true,
    numericality: true

  # Destination Id
  validates :destination_id,
    presence: true,
    numericality: true
end
