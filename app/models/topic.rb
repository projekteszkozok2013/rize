# encoding: utf-8
class Topic < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :name

  # Associations
  has_many :lessons

  # Validations
  # Name
  validates :name,
    presence: true,
    length: {maximum: 15}
end
