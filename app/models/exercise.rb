# encoding: utf-8
class Exercise < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :lesson, :type, :deadline_at, :solved_at, :solution_path,
    :reserved_at, :reserved_by, :evaluation, :grade, :finalized_at

  # Associations
  belongs_to :lesson
  belongs_to :type
  belongs_to :reserved_by, class_name: 'User'

  has_many :comments, as: :commentable

  # Validations
  # Lesson Id
  validates :lesson_id,
    presence: true,
    numericality: true

  # Type Id
  validates :type_id,
    presence: true,
    numericality: true

  # Solution Path
  validates :solution_path,
    length: {maximum: 200}

  # Evaluation
  validates :evaluation,
    length: {maximum: 10.kilobytes}

  # Grade
  validates :grade,
    allow_blank: true,
    numericality: true
end
