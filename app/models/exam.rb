# encoding: utf-8
class Exam < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :lesson, :grade, :finalized_at

  # Associations
  belongs_to :lesson

  has_many :comments, as: :commentable

  # Validations
  # Lesson Id
  validates :lesson_id,
    presence: true,
    uniqueness: true,
    numericality: true

  # Grade
  validates :grade,
    allow_blank: true,
    numericality: true

end
