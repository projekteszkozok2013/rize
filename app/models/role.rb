class Role < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :name

  # Associations
  has_and_belongs_to_many :users

  # Validations
  # Name
  validates :name,
    presence: true
end
