class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  # Accessible attributes
  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body

  # Associtaions
  has_and_belongs_to_many :roles

  def role?(role)
    return !!roles.find_by_name(role.to_s.camelize)
  end

  def add_role(role)
    roles << Role.find_by_name(role.to_s.camelize)
    return self
  end
end
