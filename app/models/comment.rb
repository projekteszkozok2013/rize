# encoding: utf-8
class Comment < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :commentable, :user, :content

  # Associations
  belongs_to :commentable, polymorphic: true
  belongs_to :user

  # Validations
  # Commentable Id
  validates :commentable_id,
    presence: true,
    numericality: true

  # Commentable Type
  validates :commentable_type,
    presence: true

  # User Id
  validates :user_id,
    presence: true,
    numericality: true

  # Content
  validates :content,
    presence: true,
    length: {maximum: 10.kilobytes}
end
