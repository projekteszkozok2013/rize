# encoding: utf-8
class Klass < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :location, :teacher, :kept_on, :description

  # Associations
  belongs_to :location
  belongs_to :teacher, class_name: 'User'

  has_many   :lessons

  # Validations
  # Location Id
  validates :location_id,
    presence: true,
    numericality: true

  # Teacher Id
  validates :teacher_id,
    presence: true,
    numericality: true

  # Kept On
  validates :kept_on,
    presence: true

  # Description
  validates :description,
    length: { maximum: 50 }

end
