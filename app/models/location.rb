# encoding utf-8
class Location < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :building, :floor, :room, :capacity

  # Associations
  has_many :klasses

  # Validations
  # Building
  validates :building,
    presence: true,
    length: { maximum: 5 }

  # Floor
  validates :floor,
    presence: true,
    length: { maximum: 15 }

  # Room
  validates :room,
    presence: true,
    length: { maximum: 15 }

  # Capacity
  validates :capacity,
    presence: true,
    numericality: true
end
