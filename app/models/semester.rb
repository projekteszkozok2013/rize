# encoding: utf-8
class Semester < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :year, :period

  # Associations
  has_many :lessons

  # Validations
  # Year
  validates :year,
    presence: true,
    uniqueness: true,
    numericality: true

  # Period
  validates :period,
    presence: true,
    uniqueness: true,
    numericality: true
end
