# encoding: utf-8
class Type < ActiveRecord::Base
  # Accessible attributes
  attr_accessible :name, :short_name

  # Associations
  has_many :exercises

  # Validations
  # Name
  validates :name,
    presence: true,
    uniqueness: true,
    length: {maximum: 40}

  # Short Name
  validates :short_name,
    presence: true,
    uniqueness: true,
    length: {maximum: 2}
end
